package com.hathy.cuartotoentregable.navegation;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.hathy.cuartotoentregable.R;

public class Navegar extends AppCompatActivity implements AdapterView.OnItemClickListener, View.OnClickListener {
    private Toolbar toolbar;

    private MyAdapter myAdapter;
    ListView nav;
    private OnFragmentInteractionListener mListener;

    String[] opcionesMenuPrincipal;
    DrawerLayout drawerLayout;
    private FragmentManager mFragmentManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navegar);
        mFragmentManager = getSupportFragmentManager();

        opcionesMenuPrincipal=getResources().getStringArray(R.array.opcionesMenuPrincipal);
         nav = (ListView)findViewById(R.id.listView);
        //nav.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,opcionesMenuPrincipal));
        nav.setOnItemClickListener(this);
        myAdapter=new MyAdapter(this);
        nav.setAdapter(myAdapter);
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        //toolbar.setBackgroundColor(getResources().getColor(R.color.primary));
        setSupportActionBar(toolbar);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(this);

        DraweFragment drawerfragment = (DraweFragment) getSupportFragmentManager().findFragmentById(R.id.fragmentdrawer);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawerlayout);

        final ActionBarDrawerToggle mToggle = new ActionBarDrawerToggle(this,                  /* host Activity */
                drawerLayout,         /* DrawerLayout object */
                toolbar,  /* nav drawer image to replace 'Up' caret */
                R.string.menuAbierto,  /* "open drawer" description for accessibility */
                R.string.menuCerrado  /* "close drawer" description for accessibility */
        ){

            public void onDrawerClosed(View view) {
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            public void onDrawerOpened(View drawerView) {
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };
        mToggle.syncState();
        drawerLayout.setDrawerListener(mToggle);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Toast.makeText(this, R.string.action_settings,Toast.LENGTH_LONG).show();
        }

        if (id == R.id.action_ayuda) {
            Toast.makeText(this, R.string.action_ayuda,Toast.LENGTH_LONG).show();
        }
        if (id == R.id.navegar) {
           startActivity(new Intent(this, Navegar2.class));
        }

        if (id == R.id.action_crear_carpeta) {
            Toast.makeText(this, R.string.action_crear_carpeta,Toast.LENGTH_LONG).show();
        }

        if (id == R.id.action_subir_archivo) {
            Toast.makeText(this, R.string.action_subir_archivo,Toast.LENGTH_LONG).show();
        }

        if (id == R.id.action_compartir) {
            Toast.makeText(this, R.string.action_compartir,Toast.LENGTH_LONG).show();
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
    }

    @Override
    public void onClick(View Buttom)
    {
        if(Buttom.getId()==R.id.fab)
        {
                Snackbar.make(Buttom, "Action activity Principal", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
        }
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onMenuSelected(int position);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
       // Toast.makeText(this, opciones[position] + "fue",Toast.LENGTH_LONG).show();
        Fragment fragment;
        drawerLayout.closeDrawers();
        switch(position) {
            default:
            case 0:
                fragment = new Opcion1();
                break;
            case 1:
                fragment = new Opcion2();
                break;
            case 2:
                fragment = new Opcion3();
                break;
            case 3:
                fragment = new Opcion4();
                break;
        }
        mFragmentManager.beginTransaction()
                .replace(R.id.container, fragment)
                .commit();

        selectItem(position);
    }

    private void selectItem(int position) {

        nav.setItemChecked(position, true);
        setTitle(opcionesMenuPrincipal[position]);
    }


    public void setTitle(String title) {
       // super.setTitle(title);
        getSupportActionBar().setTitle(title);
    }



    /*Con esta clase es que seteo los iconos y titulos del navigation drawer llamandolos desde los Recursos*/
    class MyAdapter extends BaseAdapter {
        private Context context;

        String [] socialSites;
        int[] images = {
                R.drawable.ic_folder_files32,
                R.drawable.ic_phone_team32,
                R.drawable.ic_papers32,
                R.drawable.ic_photo32};
        public MyAdapter (Context context){
            this.context = context;

            socialSites=context.getResources().getStringArray(R.array.opcionesMenuPrincipal);
        }


        @Override
        public int getCount() {
            return socialSites.length;
        }

        @Override
        public Object getItem(int position) {
            return socialSites[position];
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            View row=null;
            if (convertView==null)
            {
                LayoutInflater inflater= (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                row= inflater.inflate(R.layout.custom_row, parent,false);
            } else {
                row=convertView;

            }
            TextView titleTextview = (TextView) row.findViewById(R.id.textView);
            ImageView titleImageview= (ImageView) row.findViewById(R.id.imageView);
            titleTextview.setText(socialSites[position]);
            titleImageview.setImageResource(images[position]);
            return row;
        }
    }
}
