package com.hathy.cuartotoentregable.intro;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;

import com.hathy.cuartotoentregable.login.LoginActivity;
import com.hathy.cuartotoentregable.R;


public class Intro extends FragmentActivity{

    private ViewPager ViewPager=null;
    private TableLayout indicator;
    private Button btnOmitir, btnSiguiente;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_intro);
        ViewPager = (ViewPager)findViewById(R.id.pager);
        indicator = (TableLayout)findViewById(R.id.indicator);
        btnOmitir = (Button)findViewById(R.id.btnOmitir);
        btnSiguiente = (Button)findViewById(R.id.btnSiguiente);
        FragmentStatePagerAdapter adapter = new FragmentStatePagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                switch (position) {
                    case 0 : return new FragmentOne();
                    case 1 : return new FragmentTwo();
                    case 2 : return new FragmentThree();
                    default: return null;
                }
            }

            @Override
            public int getCount() {
                return 3;
            }
        };
        ViewPager.setAdapter(adapter);
        ViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                if (position == 2) {
                    btnOmitir.setVisibility(View.GONE);
                    btnSiguiente.setText("LISTO");
                } else {
                    btnOmitir.setVisibility(View.VISIBLE);
                    btnSiguiente.setText("ADELANTE");
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        btnOmitir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finishHim();
            }
        });

        btnSiguiente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ViewPager.getCurrentItem() == 2){
                    finishHim();
                } else {
                    ViewPager.setCurrentItem(ViewPager.getCurrentItem() + 1, true);
                }
            }
        });
    }

    private void finishHim() {
        SharedPreferences preferences =
                getSharedPreferences("Mis Preferencias", MODE_PRIVATE);

        preferences.edit()
                .putBoolean("Completado",true).apply();

        Intent principal = new Intent(this, LoginActivity.class);
        startActivity(principal);

        finish();
    }
}
