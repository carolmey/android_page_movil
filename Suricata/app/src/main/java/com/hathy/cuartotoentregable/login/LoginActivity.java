package com.hathy.cuartotoentregable.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.LayoutAnimationController;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hathy.cuartotoentregable.R;
import com.hathy.cuartotoentregable.navegation.Navegar;


public class LoginActivity extends AppCompatActivity implements Button.OnClickListener {

    private LinearLayout containerLogin, containerRegister;
    private FloatingActionButton fab, fabRegister;
    private Button btnEntrar, btnRegister;
    private TextView tvOlvidoClave;
    public EditText userName, passWord,userNameRegister, passWordRegister_repeat,passwordRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        containerLogin = (LinearLayout) findViewById(R.id.containerLogin);
        containerRegister = (LinearLayout) findViewById(R.id.containerRegister);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fabRegister = (FloatingActionButton) findViewById(R.id.fabRegister);
        btnEntrar = (Button) findViewById(R.id.btnEntrar);
        btnRegister = (Button) findViewById(R.id.btnRegister);
        tvOlvidoClave= (TextView)findViewById(R.id.tvOlvidoClave);

        userName= (EditText)findViewById(R.id.username);
        passWord= (EditText)findViewById(R.id.password);

        userNameRegister= (EditText)findViewById(R.id.usernameRegister);
        passwordRegister= (EditText)findViewById(R.id.passwordRegister);
        passWordRegister_repeat= (EditText)findViewById(R.id.passwordRegister_repeat);

        fab.setOnClickListener(this);
        fabRegister.setOnClickListener(this);
        btnEntrar.setOnClickListener(this);
        btnRegister.setOnClickListener(this);
        tvOlvidoClave.setOnClickListener(this);
    }
    /*
  * Implementando el Metodo OnClickListener
  * Capturando el Id de cada Boton de mi XML Le indico que accion va a Realizar.
  * */
    @Override
    public void onClick(View v)
    {
        if (v.getId()==R.id.fab)
        {
/*
* Implementando el Atributo Visibility Oculto las Vistas de Mi XML-FILE
* Con el SetVisivility  Cambio la Visibilidad a mi comveniencia
* En este Caso Muestro containerRegister y Oculto containerLogin
* */
            animarregister(true);
            containerLogin.setVisibility(View.GONE);
            containerRegister.setVisibility(View.VISIBLE);
            fabRegister.setVisibility(View.VISIBLE);
            fab.setVisibility(View.GONE);

        }

         if (v.getId()==R.id.fabRegister)
        {
            animar(true);
            fabRegister.setVisibility(View.GONE);
            fab.setVisibility(View.VISIBLE);

            containerLogin.setVisibility(View.VISIBLE);
            containerRegister.setVisibility(View.GONE);
        }

        if (v.getId()==R.id.btnEntrar)
        {

            String sUsername = userName.getText().toString();
            String pasdsWord = passWord.getText().toString();

            if (sUsername.matches("")) {
                Toast.makeText(this, "El Campo Usuario Es Requerido", Toast.LENGTH_SHORT).show();
                return;
            } else if (pasdsWord.matches("")) {
            Toast.makeText(this, "El Campo Contraseña Es Requerido", Toast.LENGTH_SHORT).show();

        }
            else {

                vaciarForm();
                     Intent principal = new Intent(this, Navegar.class);
                startActivity(principal);
            }


        }


        if (v.getId()==R.id.btnRegister)
        {

            String sUsernameRegister = userNameRegister.getText().toString();
            String pasdsWordRegister = passwordRegister.getText().toString();
            String pasdsWordRegister_repeat = passWordRegister_repeat.getText().toString();

            if (sUsernameRegister.matches("")) {
                Toast.makeText(this, "El Campo Usuario Es Requerido", Toast.LENGTH_SHORT).show();
                return;
            } else if (pasdsWordRegister.matches("")) {
                Toast.makeText(this, "El Campo Contraseña Es Requerido", Toast.LENGTH_SHORT).show();

            }else if (pasdsWordRegister_repeat.matches("")) {
                Toast.makeText(this, "El Campo Repetir Contraseña Es Requerido", Toast.LENGTH_SHORT).show();

            }
            else {

                vaciarForm();
                animar(true);
                containerLogin.setVisibility(View.VISIBLE);
            }


        }

        if (v.getId()==R.id.tvOlvidoClave)
        {

            vaciarForm();

            Intent principal = new Intent(this, OlvidoClave.class);
            startActivity(principal);


        }

    }

    public void  vaciarForm(){
        userName.setText("");
        passWord.setText("");
        userNameRegister.setText("");
        passwordRegister.setText("");
        passWordRegister_repeat.setText("");

    }







    /*Aqui realizo una Animacion Para la entrada de cada Vista en este Caso
    * Desde la esquina inferior derecha a la superior izquierda
    *
    * Pueden COnsultar las animaciones de Desplazamiento  en el siguiente codigo
    *
    * http://danielme.com/tip-android-12-animar-mostrarocultar-layout-con-desplazamientos/*/
    private void animar(boolean mostrar)
    {
        AnimationSet set = new AnimationSet(true);
        Animation animation = null;
        if (mostrar)
        {
            //desde la esquina inferior derecha a la superior izquierda
            animation = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 2.0f, Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF,2.0f, Animation.RELATIVE_TO_SELF, 0.0f);
        }
        else
        {    //desde la esquina superior izquierda a la esquina inferior derecha
            animation = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 2.0f, Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 2.0f, Animation.RELATIVE_TO_SELF, 0.0f);        }
        //duración en milisegundos
        animation.setDuration(500);
        set.addAnimation(animation);
        LayoutAnimationController controller = new LayoutAnimationController(set, 0.50f);

        containerLogin.setLayoutAnimation(controller);
        containerLogin.startAnimation(animation);

    }

    private void animarregister(boolean ocultar)
    {
        AnimationSet set = new AnimationSet(true);
        Animation animation = null;
        if (ocultar)
        {
            //desde la esquina inferior derecha a la superior izquierda
            animation = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 2.0f, Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 2.0f, Animation.RELATIVE_TO_SELF, 0.0f);
        }
        else
        {    //desde la esquina superior izquierda a la esquina inferior derecha
            animation = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 2.0f, Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 2.0f);
        }
        //duración en milisegundos
        animation.setDuration(500);
        set.addAnimation(animation);
        LayoutAnimationController controller = new LayoutAnimationController(set, 0.50f);


        containerRegister.setLayoutAnimation(controller);
        containerRegister.startAnimation(animation);
    }


}

